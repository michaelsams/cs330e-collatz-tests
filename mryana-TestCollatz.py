#!/usr/bin/env python3

# -------------------------------
# by Ryan Mehendale and Ajit Ramamohan
#
# Based on:
# ~~ projects/collatz/TestCollatz.py
# ~~ Copyright (C)
# ~~ Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.6/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve

# -----------
# TestCollatz
# -----------


class TestCollatz (TestCase):
    # ----
    # read
    # ----

    def test_read_1(self):
        '''Standard input'''
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1)
        self.assertEqual(j, 10)

    def test_read_2(self):
        '''Extended whitespace'''
        s = "1           10\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1)
        self.assertEqual(j, 10)

    def test_read_3(self):
        '''Single value input'''
        s = "23"
        i, j = collatz_read(s)
        self.assertEqual(i, 23)
        self.assertEqual(j, 23)

    def test_read_4(self):
        '''Blank line'''
        s = ''
        i, j = collatz_read(s)
        self.assertEqual(i, '')
        self.assertEqual(j, '')

    # ----
    # eval
    # ----

    def test_eval_1(self):
        '''Small interval'''
        v = collatz_eval(1, 10)
        self.assertEqual(v, 20)

    def test_eval_2(self):
        '''Small interval'''
        v = collatz_eval(100, 200)
        self.assertEqual(v, 125)

    def test_eval_3(self):
        '''Small, inverted interval'''
        v = collatz_eval(210, 201)
        self.assertEqual(v, 89)

    def test_eval_4(self):
        '''Small interval'''
        v = collatz_eval(900, 1000)
        self.assertEqual(v, 174)

    def test_eval_5(self):
        '''Max interval'''
        v = collatz_eval(1, 999999)
        self.assertEqual(v, 525)

    def test_eval_6(self):
        '''Single value'''
        v = collatz_eval(23, 23)
        self.assertEqual(v, 16)

    def test_eval_7(self):
        '''Large interval'''
        v = collatz_eval(123456, 654321)
        self.assertEqual(v, 509)

    def test_eval_8(self):
        '''Large, inverted interval'''
        v = collatz_eval(654321, 123456)
        self.assertEqual(v, 509)

    def test_eval_9(self):
        '''Testing boundaries of cache intervals'''
        v = collatz_eval(399999, 400999)
        self.assertEqual(v, 374)

    def test_eval_10(self):
        '''Blank line'''
        v = collatz_eval('', '')
        self.assertEqual(v, '')

    # -----
    # print
    # -----

    def test_print_1(self):
        '''Small numbers'''
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), "1 10 20\n")

    def test_print_2(self):
        '''Large numbers'''
        w = StringIO()
        collatz_print(w, 123456, 654321, 509)
        self.assertEqual(w.getvalue(), "123456 654321 509\n")

    def test_print_3(self):
        '''Single digit numbers'''
        w = StringIO()
        collatz_print(w, 1, 4, 8)
        self.assertEqual(w.getvalue(), "1 4 8\n")

    # -----
    # solve
    # -----

    def test_solve_1(self):
        '''Standard solve'''
        r = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")

    def test_solve_2(self):
        '''Extended whitespace'''
        r = StringIO("1     10\n100     200\n201    210\n900    1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")

    def test_solve_3(self):
        '''Including a single input line'''
        r = StringIO("1 10\n23\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n23 23 16\n")
# ----
# main
# ----


if __name__ == "__main__":
    main()

""" #pragma: no cover

$ cat TestCollatz.out
....................
----------------------------------------------------------------------
Ran 20 tests in 0.084s

OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          47      0     20      0   100%
TestCollatz.py      83      0      0      0   100%
------------------------------------------------------------
TOTAL              130      0     20      0   100%

"""
