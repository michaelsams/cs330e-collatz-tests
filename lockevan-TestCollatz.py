#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C)
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.6/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve

# -----------
# TestCollatz
# -----------


class TestCollatz (TestCase):
    # ----
    # read
    # ----

    def test_read_1(self):
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 1)
        self.assertEqual(j, 10)

    def test_read_2(self):
        s = "100 200\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 100)
        self.assertEqual(j, 200)

    def test_read_3(self):
        s = "201 210\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 201)
        self.assertEqual(j, 210)

    def test_read_4(self):
        s = "900 1000\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  900)
        self.assertEqual(j, 1000)

    def test_read_5(self):
        s = "20 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  20)
        self.assertEqual(j, 10)

    def test_read_6(self):
        s = "1 1\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1)
        self.assertEqual(j, 1)
    # in the case that an empty string is given, make both integers 0
    # def test_read_5(self):
    #     s = ""
    #     i, j = collatz_read(s)
    #     self.assertEqual(i, 0)
    #     self.assertEqual(j, 0)
    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = collatz_eval(1, 10)
        self.assertEqual(v, 20)

    def test_eval_2(self):
        v = collatz_eval(100, 200)
        self.assertEqual(v, 125)

    def test_eval_3(self):
        v = collatz_eval(201, 210)
        self.assertEqual(v, 89)

    def test_eval_4(self):
        v = collatz_eval(900, 1000)
        self.assertEqual(v, 174)

    def test_eval_5(self):
        v = collatz_eval(20, 10)
        self.assertEqual(v, 21)

    def test_eval_6(self):
        v = collatz_eval(1, 1)
        self.assertEqual(v, 1)

    def test_eval_7(self):
        v = collatz_eval(2, 2)
        self.assertEqual(v, 2)
    # -----
    # print
    # -----

    def test_print_1(self):
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), "1 10 20\n")

    def test_print_2(self):
        w = StringIO()
        collatz_print(w, 100, 200, 125)
        self.assertEqual(w.getvalue(), "100 200 125\n")

    def test_print_3(self):
        w = StringIO()
        collatz_print(w, 201, 210, 89)
        self.assertEqual(w.getvalue(), "201 210 89\n")

    def test_print_4(self):
        w = StringIO()
        collatz_print(w, 900, 1000, 174)
        self.assertEqual(w.getvalue(), "900 1000 174\n")

    # -----
    # solve
    # -----

    def test_solve_1(self):
        r = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")

    def test_solve_2(self):
        r = StringIO("1 5\n20 30\n31 35\n40 43\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 5 8\n20 30 112\n31 35 107\n40 43 110\n")

    def test_solve_3(self):
        r = StringIO("10 11\n111 222\n121 173\n321 432\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "10 11 15\n111 222 125\n121 173 125\n321 432 144\n")

    def test_solve_4(self):
        r = StringIO("2 7\n19 43\n37 71\n67 91\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "2 7 17\n19 43 112\n37 71 113\n67 91 116\n")

    def test_solve_5(self):
        r = StringIO("20 10\n15 13\n12 8\n5 1\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "20 10 21\n15 13 18\n12 8 20\n5 1 8\n")

    def test_solve_6(self):
        r = StringIO("1 1\n2 2\n10 10\n100 100\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 1 1\n2 2 2\n10 10 7\n100 100 26\n")

    def test_solve_7(self):
        r = StringIO("1 9999\n2 8134\n3 100000\n4 21312\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 9999 262\n2 8134 262\n3 100000 351\n4 21312 279\n")

    def test_solve_8(self):
        r = StringIO("10000 100000\n5 1023\n134 3265\n2000 2431\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "10000 100000 351\n5 1023 179\n134 3265 217\n2000 2431 183\n")

    def test_solve_9(self):
        r = StringIO("3165 3167\n10000 900\n560 10\n400 399\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "3165 3167 168\n10000 900 262\n560 10 144\n400 399 121\n")

    def test_solve_10(self):
        r = StringIO("513 436\n12 12\n333 333\n400 400\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "513 436 142\n12 12 10\n333 333 113\n400 400 28\n")

    def test_solve_11(self):
        r = StringIO("300 300\n111 111\n222 222\n444 444\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "300 300 17\n111 111 70\n222 222 71\n444 444 72\n")

    def test_solve_12(self):
        r = StringIO("20 20\n30 30\n40 40\n50 50\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "20 20 8\n30 30 19\n40 40 9\n50 50 25\n")

    def test_solve_13(self):
        r = StringIO("21 11\n32 5\n18 3\n5 1\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "21 11 21\n32 5 112\n18 3 21\n5 1 8\n")

    def test_solve_14(self):
        r = StringIO("1 999999")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 999999 525\n")
# ----
# main
# ----


if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch TestCollatz.py >  TestCollatz.out 2>&1


$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


$ coverage report -m                   >> TestCollatz.out



$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          12      0      2      0   100%
TestCollatz.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
